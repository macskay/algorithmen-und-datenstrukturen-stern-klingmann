# -*- coding: cp1252 -*-
#Klingmann,Sch�npflug,Stern
import random
import timeit
from math import sqrt


#createData erstellt eine Liste mit Zufallszahlen von 0 bis 1
def createData(size):
    a=[]
    while len(a) < size:
        x,y=random.uniform(-1,1),random.uniform(-1,1)
        r = sqrt(x**2 + y**2)
        if r < 1.0:
            a.append(r)
    return a

#bucketMap entscheidet, in welches Bucket die Zahl r geschrieben wird
def bucketMap(r,m): 
    if naiv:
        index = int(r*m)
    else:
        index = int(r**2*m)
    return index


#bucketSort erstellt buckets und sortiert ein
def bucketSort(array,m):
    #buckets werden erstellt
    buckets=[]
    for i in range(m):
        buckets.append([])

    #Buckets werden bef�llt
    for i in range(len(array)):
        index = bucketMap(array[i],m) #Welches Bucket?
        buckets[index].append(array[i])
    if time==False:
        test_result = chiTest(buckets,size,c)
    for i in xrange(len(buckets)):
        insertionSort(buckets[i])
    
    result = bucketJoin(buckets)
    return result


#Komplettes Buckets-Array ausgeben
def printBuckets(array):
    m=0
    for i in array:
        print "Bucket ",m,i
        m +=1

#Sortiert die bucket-Eintr�ge mit insertionSort        
def insertionSort(array):
    for i in range(1, len(array)):
        t = array[i]
        for j in range(0, i):
            if(t < array[j]):
                z = array[j]
                array[j] = t
                t = z
        array[i] = t

#Verbindet die Buckets in einer Liste   
def bucketJoin(array):
    joined=[]
    for i in array:
        joined +=i
    return joined

#chiTest testet, ob die Abweichung von bucketMap eingehalten wurde
def chiTest(array,a,b): #array=buckets, a=size, b=c
    result=False
    chi=0.0
    for i in range(len(array)):
        eV=float(a/b)          #expected Value
        oV=float(len(array[i]))#observed Value
        chi +=float((oV-eV)**2/eV)
    print "Chi-Result:",chi
    tau=sqrt(2*chi)-sqrt(2*b-3)
    if abs(tau) > 3:
        print "|",tau,"|","> 3 => Nicht gleichm��ig verteilt"
    else:
        print "|",tau,"|","< 3 => gleichm��ig verteilt"
        result=True
    return result

#BucketSort Einstellungen-------------------------------------------
time = False      #Timit-Tests werden nicht ausgef�hrt bei False
#-------------------------------------------------------------------
if time:
    do="bucketSort(liste,c)"
    init1 = """
size=18
c=3
import random
from __main__ import createData
from __main__ import bucketSort
liste = createData(size)
"""
    init2 = """
size=180
c=30
import random
from __main__ import createData
from __main__ import bucketSort
liste = createData(size)
"""
    init3 = """
size=1800
c=300
import random
from __main__ import createData
from __main__ import bucketSort
liste = createData(size)
"""
    naiv=True
    t1=timeit.Timer(do,init1)
    t2=timeit.Timer(do,init2)
    t3=timeit.Timer(do,init3)
    print 'bucketMap Optimiert:'
    print 'Zeit: ',t1.timeit(10)/10
    print 'Zeit: ',t2.timeit(10)/10
    print 'Zeit: ',t3.timeit(10)/10
    naiv=True
    print 'bucketMap Standard:'
    print 'Zeit: ',t1.timeit(10)/10
    print 'Zeit: ',t2.timeit(10)/10
    print 'Zeit: ',t3.timeit(10)/10

    
else:
    size=100
    c=5
    print 'bucketMap Optimiert:' 
    naiv=False
    liste = createData(size)
    bucketListe = bucketSort(liste,c)
    print
    print 'bucketMap Standard:'
    naiv=True
    liste = createData(size)
    bucketListe = bucketSort(liste,c)

print
print "Offensichtlich ist, dass es linear steigt und auch das bucketMap nicht stark ins Gewicht faellt."
print "Die optimierte Formel zur Schl�ssel-Verteilung lautet: r^2*m. Durch das Quadrieren werden die Zahlen arithmetisch gemittelt, da viele gro�e Zahlen weiter nach unten r�cken."
print "Bemerkung: Im Code time auf True setzen, damit timit-Module ausgef�hrt werden."

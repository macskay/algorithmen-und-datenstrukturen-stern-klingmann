#Klingmann, Stern, Sch�npflug

# -*- coding: cp1252 -*-
import unittest
import random
from Treap import *
class MeinTreapTest(unittest.TestCase):
    def setUp(self):
        self.root=Node(5, random.randint(1,1000))

    def test_randomTreapInsert(self):
        temp = randomTreapInsert(self.root, 4)      #4 wird eingef�gt
        if self.root.prio > temp.prio:              #Wenn die Wurzel hr��ere Prio hat
            self.assertEqual(self.root.left.key, 4) #wird die 4 das linke Kind
        else:                                       #sonst
            self.assertEqual(temp.right.key, 5)     #wird Wurzel rechtes Kind


        temp2 = randomTreapInsert(self.root, 4)     #Falls noch eine 4 eingef�gt wird
        self.assertEqual(temp.key, temp2.key)       #passiert nichts 
  
    def test_dynamicInsert(self):
        self.root = Node(5,0)
        temp = dynamicTreapInsert(self.root, 4)     #4 wird eingef�gt
        self.assertEqual(self.root.left.key, 4)

        temp2 = dynamicTreapInsert(self.root, 4)     #Falls noch eine 4 eingef�gt wird
        self.assertEqual(temp2.prio, 1)              #wird die Priorit�t inkrementiert

    def test_rotateRight(self):
        root=Node(5,0)
        nd1=Node(3,0)
        root.left = nd1
        
        nd2=Node(7,0)
        root.right = nd2
        
        nd3=Node(4,0)
        nd1.right = nd3
        
        nd4=Node(6,0)
        nd2.left = nd4
        
        temp=rotateRight(root)                      #3 wird nach rechts rotiert
        self.assertEqual(temp.right.key, 5)         #5 muss rechtes Kind von 4 sein
        self.assertEqual(temp.right.left.key, 4)    #und ehemaliges rechtes Kind von 3 (4) ist jetzt linkes Kind von 5
    
    def test_rotateLeft(self):
        root=Node(5,0)
        nd1=Node(3,0)
        root.left = nd1
        
        nd2=Node(7,0)
        root.right = nd2
        
        nd3=Node(4,0)
        nd1.right = nd3
        
        nd4=Node(6,0)
        nd2.left = nd4
        
        temp=rotateLeft(root)                       #7 wird nach links rotiert
        self.assertEqual(temp.left.key, 5)          #5 muss linkes Kind von 7 sein
        self.assertEqual(temp.left.right.key, 6)    #und ehemaliges linkes Kind von 7(6) ist jetzt rechtes Kind von 5
    
    def test_checkSort(self):                       #Bei gleichen Eintragungen wird Prio hochgesetzt
        root=Node(5,0)
        nd1 = dynamicTreapInsert(root, 5)
        nd2 = dynamicTreapInsert(root, 5)
        prio=checkSort(root)
        self.assertEqual(prio,2)

    #Unittest f�r randomBaumErstellen und dynamicBaumErstellen sind trivial,
    #da dort nur wiederholt dynamicTreapInsert bzw. randomTreapInsert aufgerufen wird,
    #welche schon getestet wurden
   
        
    
if __name__ == '__main__':
    unittest.main()
    
    

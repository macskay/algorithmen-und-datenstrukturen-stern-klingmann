#Klingmann, Stern, Sch�npflug
# -*- coding: cp1252 -*-

#Aufgabenteile a und b)
import random
import sys
random.seed()

class Node:
    def __init__(self,key,prio):
        self.key = key      #Schl�ssel
        self.left = None    #linkes Kind
        self.right = None   #reches Kind
        self.prio = prio   #Priorit�t

def randomTreapInsert(node, key):
    if node is None:      # richtiger Platz gefunden
        return Node(key, random.randint(1,10000))  # => neuen Knoten einf�gen
    elif node.key == key:   # schon vorhanden
        return node       # => nichts tun
    elif key < node.key:
        node.left = randomTreapInsert(node.left, key) # im linken Teilbaum einf�gen
        if node.left.prio > node.prio:
            node = rotateRight(node)
        return node        
    else:
        node.right = randomTreapInsert(node.right, key) # im rechten Teilbaum einf�gen
        if node.right.prio > node.prio:
            node = rotateLeft(node)
        return node

def dynamicTreapInsert(node, key):
    if node is None:      # richtiger Platz gefunden
        return Node(key,0)  # => neuen Knoten einf�gen
    elif node.key == key:   # schon vorhanden
        node.prio += 1
        return node       # => nichts tun
    elif key < node.key:
        node.left = dynamicTreapInsert(node.left, key) # im linken Teilbaum einf�gen
        if node.left.prio > node.prio:
            node = rotateRight(node)
        return node        
    else:
        node.right = dynamicTreapInsert(node.right, key) # im rechten Teilbaum einf�gen
        if node.right.prio > node.prio:
            node = rotateLeft(node)
        return node
    
def rotateRight(node):          #Rechts-Rotation
    newRoot = node.left
    node.left = newRoot.right
    newRoot.right = node
    return newRoot

def rotateLeft(node):           #Links-Rotation
    newRoot = node.right
    node.right = newRoot.left
    newRoot.left = node
    return newRoot

def checkSort(root, count):            #Gibt die Priorit�ten aus
        if count <= 6:
            count += 1
            print
            if root is not None:
                print "Vater:",root.key,"| Priorit�t:",root.prio
                if root.left is not None:
                    print "Linkes Kind:",root.left.key,"| Priorit�t:", root.left.prio
                else:
                    print "Linkes Kind None"
                if root.right is not None:
                    print "Rechtes Kind:",root.right.key,"| Priorit�t:", root.right.prio
                else:
                    print "Rechtes Kind None"
                checkSort(root.left, count)
                checkSort(root.right, count)
    
def printBaum(root):            #Gibt den kompletten Baum aus
    if root is not None:
        print
        print "Vater:",root.key
        if root.left is not None:
            print "Linkes Kind",root.left.key
        else:
            print "linkes Kind None"
        if root.right is not None:
            print "Rechtes Kind",root.right.key
        else:
            print "rechtes Kind None"
        print "----------------------------"
        printBaum(root.left)
        printBaum(root.right)

def randomBaumErstellen(wurzel,array):        #erstellt den ganzen random Baum
    for i in range(len(array)):  
        wurzel = randomTreapInsert(wurzel,array[i])
    return wurzel

def dynamicBaumErstellen(wurzel,array):        #erstellt den ganzen dynamic Baum
    for i in range(len(array)):  
        wurzel = dynamicTreapInsert(wurzel,array[i])
    #checkSort(wurzel)
    return wurzel

############ ZUR AUSGABE FOLGENDES AUSKOMMENTIEREN #########

a=[4,8,8,5,8,1,3]                       #Keys
root=Node(5,random.randint(1,1000))     #Wurzel wird erstellt
root = randomBaumErstellen(root, a)           
printBaum(root)

root2=Node(5,0)
root2=dynamicBaumErstellen(root2, a)


#Aufgabenteil c)
#------------------------------------------------------------------------------
def fileEinlesen():
    try:
        d = open("die-drei-musketiere.txt").read()  #File einlesen        
        for k in ',;.:-"\'!)':
            d = d.replace(k, '')
        d = d.lower()
        text = d.split()
        
        randomTreap = None
        dynamicTreap = None
        for word in text:
            randomTreap  = randomTreapInsert(randomTreap, word)
            dynamicTreap = dynamicTreapInsert(dynamicTreap, word)
        return randomTreap, dynamicTreap
    except:
        print "Die Datei konnte nicht verarbeitet werden!"
        sys.exit(0)

count=0
rTreap, dTreap = fileEinlesen()
checkSort(dTreap, count)

#Aufgabenteil d)
#------------------------------------------------------------------------------
def treeDepth(node):
    if node is None:
        return 0            #Keine Tiefe, da gar kein Knoten
    elif node.left is None and node.right is None:
        return 1            #Tiefe = 1, da nur eine Wurzel enthalten
    return max(treeDepth(node.left), treeDepth(node.right)) + 1

def knotenZaehlen(node):
    if node is None:
        return 0            #Keine Knoten
    elif node.left is None and node.right is None:
        return 1            #Knoten = 1, da nur eine Wurzel enthalten
    return knotenZaehlen(node.left) + knotenZaehlen(node.right) +1

print knotenZaehlen(rTreap)
print treeDepth(dTreap)


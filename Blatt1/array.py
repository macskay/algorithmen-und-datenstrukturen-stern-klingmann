# Julien Stern & Max Klingmann
# Tutor: Kai Karius

class DynamicArray:
    def __init__(self):
        self.capacity = 1
        self.data = [None]*self.capacity
        self.size = 0

    def __getitem__(self, index):
        if index < 0 or index >= self.size:
            print "Index out of Range"
            return None
        return self.data[index]

    def append(self, item):
        if self.capacity == self.size:
            self.data = self.data + [None]*self.capacity
            self.capacity *= 2
            
            print "Post-Capacity:",self.capacity
            print "Post-Size:",self.data
            print 
            
        self.data[self.size] = item
        self.size += 1


array = DynamicArray()
array.append(1)
array.append(2)
array.append(3)
array.append(4)
array.append(5)
array.append(6)
array.append(7)
array.append(1337)
array.append(8)
array.append(9)

print "Get Item at index 7:",array.__getitem__(7)

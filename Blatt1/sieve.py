# Julien Stern & Max Klingmann
# Tutor: Kai Karius

#Array mit n Zahlen

def sieve(maxNumber):
    #creating empty list & a for-loop filling that list with numbers from 0 to maxNumber+1
    z = []
    for i in range(maxNumber+1):
        z.append(1)

    #setting inidices of 0 and 1 False, so they don't appear in the list
    z[0] = 0
    z[1] = 0

    #starting a for-loop with start-index 2, because the primenumbers start with that index
    #if z[i] is 1 or TRUE take all the multiples and set them FALSE, since they can't be prime numbers
    for i in range(2,maxNumber):
        if i:            
           for j in range(i+i,maxNumber+1,i):
                z[j] = 0

    #print out the indices, in which the content of the array is still TRUE, these are the primenumbers!
    for i in range(maxNumber+1):
        if z[i]==1:
            print i
            
    return z

print "primes = sieve(1000):"
primes = sieve(1000)

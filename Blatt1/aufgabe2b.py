
import copy

a = [1,2,3,4]               #Liste wird erzeugt
b = a                       #Eine neue Referenz wird auf a erzeugt, da NICHT einfacher Datentyp
c = copy.deepcopy(a)        #Neues Objekt, mit gleichen Werten wie a, da Tiefenkopie erfolgt

print a == b, a == c, b == c  #Werte werden verglichen und darum, sind alle 3 true!
print a is b, a is c, b is c  #1. Ausgabe ist True, da Referenzen; 2. & 3. Ausgabe ist False, da anderes Objekt 

a[0] = 42

print a == b, a == c, b == c  #1. Ausgabe True, da a ver�ndert -> b ver�ndert (Referenz);
                              #2. & 3. False, da erneut anderes Objekt. (Eine �nderung ist nicht ausschlaggebend f�r c)
print a is b, a is c, b is c  #1. Ausgabe True, da gleiche Referenz; 2. & 3. Ausgabe False, da anderes Objekt
print a[0],b[0],c[0]          #Da a und b auf das gleiche Objekt referenzieren -> gleiche Ausgabe
                              #Da c ein neues Objekt (Kopie) ist, wurde durch die Ver�nderung a[0] = 42 nichts bewirkt
                              #und der Inhalt ist wie am Anfang gleich geblieben


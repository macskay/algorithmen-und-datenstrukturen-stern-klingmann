# Julien Stern & Max Klingmann
# Tutor: Kai Karius

Bookmark:
����Vorteile: wenig Speicherbedarf, immer aktuell
����Nachteile: nur im Online-Betrieb, falls Seite down ist -> kein Zugriff auf Daten

Seiten-Inhalt speichern:
����Vorteile: auch im Offline-Betrieb nutzbar, wenn Seite gel�scht wird ensteht kein Datenverlust
����Nachteile: viel Speicherbedarf, nicht immer aktuell

# -*- coding: cp1252 -*-
#Julien Stern & Max Klingmann (Tutor: Kai Karius)



############################################## HAUPTPROGRAMM ######################################################################

import sys

class Deque:
    #Konstruktor
    def __init__(self, N):
        try:
            if N <= 0:
                raise RuntimeError("Maximalkapazit�t muss gr��er 0 sein!")
            if type(1) != type(N):
                raise RuntimeError("Es muss eine Ganzzahl als Maximalkapazit�t eingegeben werden!")
            else:    
                self.cap=int(N)
                self.sz=0
                self.front=0
                self.back=0
                self.deque=[None]*self.cap
        except RuntimeError as e:
            print "Fehler:",e
            

    def __str__(self):
        return str(self.deque)
    
    #Maximalkapazit�t
    def capacity(self):
        return self.cap

    #Tats�chliche Gr��e 
    def size(self):
        return self.sz

    #Hinzuf�gen am Ende der Liste
    def push(self,x):
        if self.sz == 0:
            self.deque[self.back] = x
            self.sz+=1 
        else:
            if self.back != self.cap-1:
                if self.back == self.front-1:
                    if self.front == self.cap-1:
                        self.front=0
                    else:
                        self.front+=1
                self.back += 1
                self.deque[self.back] = x
                self.sz+=1
            else:
                self.back = 0
                self.front += 1
                self.deque[self.back] = x
        return self.deque
              

    #Erstes Element auslesen
    def popFirst(self):
        try:
            if self.sz > 0:
                if self.sz == 1:
                    self.front = 0
                    return self.deque[self.front]
                if self.deque[self.front] == None:
                    raise RuntimeError("Es ist kein Element mehr vorhanden!")
                else:
                    temp=self.deque[self.front]
                    self.deque[self.front]=None
                    if self.front == self.cap-1:
                        self.front = 0
                    else:
                        self.front += 1
                        
                    return temp
            else:
                raise RuntimeError("Es ist kein Element mehr vorhanden!")
        except RuntimeError as e:
            return e
            

    #Letztes Element auslesen
    def popLast(self):
        try:
            if self.sz == 1:
                self.back = 0
                return self.deque[self.back]
            if self.deque[self.back] == None:
                raise RuntimeError("Es ist kein Element mehr vorhanden!")
            else:
                temp=self.deque[self.back]
                self.deque[self.back]=None
                if self.back == 0:
                    self.back = self.cap-1
                else:
                    self.back -= 1
                
                return temp
        except RuntimeError as e:
            return e



def main():
    try:
        q=Deque(5)
        q.push(1) #Front-Index: 0, Back-Index:0
        q.push(2) #Front-Index: 0, Back-Index:1
        q.push(3) #Front-Index: 0, Back-Index:2
        q.push(4) #Front-Index: 1, Back-Index:0

        ######## Testbeginn #######
        q.push(5) #Front-Index: 2, Back-Index:1
        q.push(6) #Front-Index: 0, Back-Index:2
        q.push(7) #Front-Index: 1, Back-Index:0
        print "Standard-Array nach Initialisierung (7x gepusht)"
        print q
        print
        print "Pop-First"
        print q.popFirst() #5 f�llt raus (Front-Index verschiebt sich um 1 nach rechts -> Front-Index:2, Back-Index bleibt)
        print q
        print
        print "Push 8"
        q.push(8)          #Neue Zahl (Back-Index verschiebt sich um 1 nach rechts -> Back-Index:1, Front-Index bleibt)
        print q
        print
        print "Pop-Last"
        print q.popLast()  #8 f�llt raus (Back-Index verschiebt sich um 1 nach links -> Back-Index wieder 0)
        print q
        print
        print "Pop-First"
        print q.popFirst()
        print q
        print
        print "Pop-Last"
        print q.popLast()
        print q
        print
        print "Pop-First"
        print q.popFirst()
        print q
        print
        print "Pop-Last"
        print q.popLast()
        print q
        print
        print "Pop-First"
        print q.popFirst()
        print q
        print
        print "Pop-Last"
        print q.popLast()
        print q
        
        
    except:
        print



#################################################### DEBUG ################################################################

        
import unittest

class MeinTest(unittest.TestCase):
    q=Deque(5)
    k=3 
    def setUp(self):
        for i in range(self.k):
            self.q.push(i)

    def tearDown(self):
        for i in range(len(self.q.deque)):
            self.q.deque[i]=None
        self.q.cap=int(5)
        self.q.sz=0
        self.q.front=0
        self.q.back=0
        self.q.deque=[None]*self.q.cap
        
    def testPush(self):
        print "Push-Test",self.q.deque
        self.assertEqual(self.q.push(self.q),self.q.deque)
        
    def testConst(self):
        print "Const-Test",self.q.deque
        self.assertEqual(type(self.q.cap),type(4))  #�berpr�fung auf Integer
        self.assertGreater(self.q.cap,0)            #�berpr�fung auf > 0
        self.assertEqual(len(self.q.deque),self.q.cap) #�berpr�fung auf L�nge des Arrays -> Muss N gro� sein
    
    def testSize(self):
        print "size()-Test",self.q.deque
        self.assertEqual(self.q.sz,self.k)                 #Nachbedingung: self.q.sz muss k entsprechen, durch k-maliges pushen

    def testCapacity(self):
        print "capacity()-Test",self.q.deque
        self.assertEqual(self.q.cap,5)                     #Nachbedingung: Ist die Kapazit�t wirklich 5?

    def testPopFirst(self):
        print "popFirst()-Test",self.q.deque
        self.assertGreater(self.q.size(),0)           #Vorbedingung: sz > 0
        temp1=self.q.sz
        element=self.q.popFirst()
        self.assertEqual(self.q.sz,temp1)             #Nachbedingung: sz -= 1
        self.assertNotIn(element,self.q.deque)      #Nachbedingung: �berpr�fung auf Inhalt

    def testPopLast(self):
        print "popLast()-Test",self.q.deque
        self.assertGreater(self.q.size(),0)           #Vorbedingung: sz > 0
        temp1=self.q.sz
        element=self.q.popLast()
        self.assertEqual(self.q.sz,temp1)             #Nachbedingung: sz -= 1
        self.assertNotIn(element,self.q.deque)
        
        
        
if __name__ == "__main__":
    unittest.main()



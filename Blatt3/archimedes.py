# -*- coding: cp1252 -*-
#Alda-�bungsblatt 3 Klingmann, Stern
from math import sqrt


k=30 #Anzahl der Verdoppelungen

#Implementierung des ersten Algorithmus:
def archimedes1(k):
    n=4
    s=sqrt(2)
    t=float(2.0)
    for i in range(k):
        print i,"te Verdoppelung"
        print n,"- Eck"
        print "Inner:",s*(n/2)
        print "Outer:",t*(n/2)
        print "Differenz der Schaetzwerte:",diff(s,t)

        verify_t=verify(s)

        print "t nach Formel:",t
        print "t nach verify:",verify_t

        if feq(verify_t,t):
            print "Verifizierung erfolgreich"
        else:
            print "Verifizierung fehlgeschlagen"

        s=inner1(s)
        t=outer1(t)
        n*=2
        print ""
        
#Funktion, f�r inneres n-Eck
def inner1(s):
    erg=sqrt(2-sqrt(4-(s*s)))
    return erg

#Funktion, f�r �u�eres n-Eck
def outer1(t):
    erg=(2/t)*(sqrt(4+(t*t))-2)
    return erg
#Funktion, welche Differenz zwischen den beiden errechneten Werten berechnet
def diff(s,t):
    return t-s

#Implementierung des zweiten Algorithmus:
def archimedes2(k):
    n=4
    s=sqrt(2)
    t=float(2)
    for i in range(k):
        print i,"te Verdoppelung"
        print n,"- Eck"
        print "Inner:",s*(n/2)
        print "Outer:",t*(n/2)
        print "Differenz der Schaetzwerte:",diff(s,t)

        verify_t=verify(s)

        print "t nach Formel:",t
        print "t nach verify:",verify_t

        if feq(verify_t,t):
            print "Verifizierung erfolgreich"
        else:
            print "Verifizierung fehlgeschlagen"
        

        s=inner2(s)
        t=outer2(t)
        n*=2
        print ""
        
#Funktion, f�r inneres n-Eck Nr2:
def inner2(s):
    erg=s/sqrt(2+sqrt(4-(s*s)))
    return erg

#Funktion, f�r �u�eres n-Eck Nr2:
def outer2(t):
    erg=2*t/(sqrt(4+(t*t))+2)
    return erg
#Aufgrund der Ungenauigkeit von float-Werten muss eine eigens entwickelte Funktion f�r boolsche Vergleiche benutzt werden
def feq(a,b):
    return abs(a-b)<=1e-15
    
#Verifikation von t �ber s:
def verify(s):
    t_intern=(2*s)/(sqrt(4-(s*s)))
    return t_intern


archimedes1(k)
#archimedes2(k)


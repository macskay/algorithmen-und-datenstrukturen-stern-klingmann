# -*- coding: cp1252 -*-
#Alda-Übungsblatt 3 Klingmann, Stern
import unittest
import sortierverfahren
import random
import copy
class MeinTest(unittest.TestCase):

    liste = range(10)
    liste1 = copy.deepcopy(liste)
    liste2 = copy.deepcopy(liste)
    #backUpListe dient als Sicherung sortierten Liste
    backUpListe=copy.deepcopy(liste)

    #x==return-Wert 1 von selectionSort, y==ComparisonCount(keine Verwendung)
    x,y=0,0
    a,b=0,0
    c,d=0,0
    
    def setUp(self):
        #Liste wird gemischt und ausgegeben
        random.shuffle(self.liste)
        print "Random-Liste:",self.liste
        self.x,self.y=sortierverfahren.selectionSort(self.liste)
        print "selectionSort sortiert:",self.x

        self.a,self.b=sortierverfahren.mergeSort(self.liste1,self.b)
        print "mergeSort sortiert:",self.a        

        self.c,self.d=sortierverfahren.quickSort(self.liste2,0,len(self.liste2)-1,self.d)
        print "quickSort sortiert:",self.c  

    
    def testBerechnung(self):
        self.assertEqual(self.x,self.backUpListe)
        self.assertEqual(self.a,self.backUpListe)
        self.assertEqual(self.x,self.backUpListe)

if __name__== "__main__":
    unittest.main()

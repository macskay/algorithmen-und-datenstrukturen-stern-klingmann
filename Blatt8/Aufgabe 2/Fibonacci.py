# -*- coding: cp1252 -*-
# Klingmann, Stern, Sch�npflug


#Baumrekursiv
def fibTreeRec(n):
    if n<=1:
        return n
    return fibTreeRec(n-1) + fibTreeRec(n-2)


#Course-Of-Value-Rekursiv
def fibCOVImpl(n):
    if n == 0:
        return 1, 0
    else:
        
        f1, f2 = fibCOVImpl(n-1)
        return f1 + f2, f1

def fibCOV(n):
    f1, f2 = fibCOVImpl(n)
    return f2

#Iterativ
def fibIter(n):
    f1, f2 = 1, 0
    while n > 0:
        f1, f2 = f1 + f2, f1
        n -= 1
    return f2

#Wurde nur zum Testen verwendet. F�r die Ausgabe wurde die Eingabe hardcoded
def eingabe():
    try:
        return int(input("Bitte eine Ganzzahl eingeben:"))
    except:
        print "Keine Ganzzahl eingegeben!"

################## AUFGABENTEIL 2b ##################

#Multiplikation der Matrizen. Eine 2x2-Matrix wird hier durch ein 3er-Tupel dargestellt
#Bsp.: A=
#  ( a, b )
#  ( b, c )
def fibMultiplikation(mat1, mat2):
    a, b, c = mat1
    d, e, f = mat2
    return a*d + b*e, a*e + b*f, b*e + c*f

def einheitsMatrix():
    return (1, 0, 1)

def potenzBerechnen(matrix, n):
    if n == 1:
        return matrix
    #elif n == 0:
    #    return einheitsMatrix()
    elif (n%2)==0:
        return potenzBerechnen(fibMultiplikation(matrix, matrix), n/2)
    else:
        return fibMultiplikation(matrix, potenzBerechnen(fibMultiplikation(matrix, matrix), (n-1)/2))

def fibMatrix(n):
    if n < 2:
        return n
    return potenzBerechnen((1,1,0),n-1)[0]



################# TEST-BEREICH #####################

if __name__ == "__main__":
    from timeit import Timer
    approx=u"\u2248" #Erstellen des "Fast gleich"-Zeichens
    
    #Baumrekursion
    #-------------
    zeit=0
    zahlTree=0
    print "Bitte warten, w�hrend die gr��tm�gliche Zahl errechnet wird..."
    try:
        while zeit < 10:
            t3 = Timer('fibTreeRec(p1)', "from __main__ import fibTreeRec; p1=%d" % zahlTree)
            zeit=t3.timeit(1)
            zahlTree+=1
    except:
        print "Maximale Rekursionsaufrufe bei",zahlTree,"erreicht!"
    #Typkonvertierung, damit der Punkt ohne Leerzeichen angef�gt wird
    print "Mit der Baumrekursion kommt man in der gegebenen Zeit maximal bis zur",str(zahlTree) + ". Rekursion"
    print "und diese dauert",approx,zeit," Sekunden!"

    print "----------------------------------------------------"  
    #Course-Of-Value-Rekursion
    #-------------------------
    zeit=0
    zahlCOV=0
    print "Bitte warten, w�hrend die gr��tm�gliche Zahl errechnet wird..."
    try:
        while zeit < 10:
            t3 = Timer('fibCOV(p1)', "from __main__ import fibCOV; p1=%d" % zahlCOV)
            zeit=t3.timeit(1)
            zahlCOV+=1
    except:
        print "Maximale Rekursionsaufrufe bei",zahlCOV,"erreicht!"
    #Typkonvertierung, damit der Punkt ohne Leerzeichen angef�gt wird
    print "Mit der Course-Of-Value-Rekursion kommt man in der gegebenen Zeit maximal bis zur",str(zahlCOV) + ". Rekursion"
    print "und diese dauert",approx,zeit," Sekunden!"

    print "----------------------------------------------------"  
    #Iteration
    #---------
    zeit=0
    zahlIter=254000
    print "Bitte warten, w�hrend die gr��tm�gliche Zahl errechnet wird..."
    while zeit < 10:
       t3 = Timer('fibIter(p1)', "from __main__ import fibIter; p1=%d" % zahlIter)
       zeit=t3.timeit(1)
       zahlIter+=1000
    print "Mit der iterativen Methode kommt man in der gegebenen Zeit maximal bis zur",str(zahlIter) + ". Iteration"
    print "und diese dauert",approx,zeit," Sekunden!"

    print "----------------------------------------------------"  
    #Gleicheits-Test
    print "Gleichheits-Test"
    print "fibIter(22):",fibIter(22)
    print "fibMatrix(22):",fibMatrix(22)

    print "----------------------------------------------------"
    #Matrix-Potenzen
    #---------------
    zeit=0
    zahlPot=2500000
    print "Bitte warten, w�hrend die gr��tm�gliche Zahl errechnet wird..."
    while zeit < 10:
       t3 = Timer('fibMatrix(p1)', "from __main__ import fibMatrix; p1=%d" % zahlPot)
       zeit=t3.timeit(1)
       zahlPot+=10000
       print zeit
    print "Mit der Potenzmatrix-Methode kommt man in der gegebenen Zeit maximal bis zur",str(zahlPot) + ". Iteration"
    print "und diese dauert",approx,zeit," Sekunden!"



#Erkl�rung:
#   Die Baumrekursion ist mit Abstand die langsamste Berechnung der drei gezeigten M�glichkeiten, da durch
#   den rekursiven Aufruf die Berechnungszeit jeweils immer vervielfacht wird.
#   Bei der Course-Of-Value-Rekursion wurde die Berechnung verschnellert, es kommt jedoch ab dem 993. rekursiven
#   Aufruf zur "maximum recursion exceeded"-Fehlermeldung, somit ist die Variante nur begrenzt vorteilhafter,
#   als die Baumrekursion.
#   Mit Hilfe der iterativen Methode kommen wir ein St�ck weiter und k�nnen in 10 Sekunden Die Fibonaccizahlen bis
#   zu n=25500 errechnen lassen ohne gro�en Zeitaufwand. Das ist darauf zur�chzuf�hren, dass die Funktionen
#   nicht rekursiv verschachtelt aufgerufen werden m�ssen, sondern jede Berechnung allein f�r sich erfolgt.    

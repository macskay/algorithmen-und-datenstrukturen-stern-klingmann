# -*- coding: cp1252 -*-
#Klingmann,Stern,Schönpflug
from math import sqrt
import sys,os,copy

#Punkt a:
aP=[0.0,0.0]
#Punkt b:
bP=[1.0,0.0]
#Punkt c:
cP=[1.0/2.0,sqrt(3.0)/2.0]

globArray=[aP,bP,cP,None]

def kochSnowFlake(level):
    #aP ..... bP ..... cp ......
    if level > 0:
        newArray=[]
        global globArray
        for i in range(len(globArray)-1):
            newArray.append(globArray[i]) #aP
            if globArray[i+1] is not None:
                b,c,d=koords(globArray[i],globArray[i+1])
            else:
                b,c,d=koords(globArray[i],globArray[0])    
            newArray.append(b)  #b
            newArray.append(c)  #c
            newArray.append(d)  #d

        newArray.append([0.0,0.0])
        globArray=copy.deepcopy(newArray)    
        del newArray[:]
    
        kochSnowFlake(level-1)
    
def koords(a,e):
    #Berechnet Koordinaten von b,c,d, wobei c die Spitze des neuen Dreiecks bildet
    vec=[e[0]-a[0],e[1]-a[1]]
    l_vec=laenge(a,e)
    b=[a[0]+vec[0]/3,a[1]+vec[1]/3]
    d=[a[0]+(vec[0]/3)*2,a[1]+(vec[1]/3)*2]

    mitte=[b[0]+vec[0]/6,b[1]+vec[1]/6]
    #print "mitte", mitte

    ortho=orth(vec)
    #print "ortho",ortho

    hoehe=sqrt((l_vec/3)**2-(l_vec/6)**2)
    #print "hoehe",hoehe

    norm_ortho=normVec(ortho,l_vec)
    #print "norm_ortho",norm_ortho

    c=[mitte[0]+norm_ortho[0]*hoehe,mitte[1]+norm_ortho[1]*hoehe]
    return b,c,d    

def normVec(x,l):
    norm=[x[0]/l,x[1]/l]
    return norm

def orth(x):
    #s,t => t,-s
    orthVec=[x[1],x[0]*(-1)]     
    return orthVec 

def laenge(g,h):
    return sqrt((h[0]-g[0])**2.0+(h[1]-g[1])**2.0)

def zeichnen(array):
    try:
        d=open("points.txt","w+")
    except:
        print("Datei nicht gefunden")
        sys.exit(0)
    
    #Schreiben aus dem Array in die Datei
    for i in array:
        x=str(i[0])+" "
        y=str(i[1])+"\n"
        d.write(x)
        d.write(y)
    d.close()

print "=============================================================="
print "Unterteilungs-Regeln:"
print "Die drei neuen Punkte b,c,d werden so berechnet:"
print "b := [a[0]+vec[0]/3,a[1]+vec[1]/3], wobei \"vec\" der Vektor ae ist und \"a\" der Anfangspunkt"
print "d := [a[0]+(vec[0]/3)*2,a[1]+(vec[1]/3)*2], also gerade ein Drittel von \"vec\" weiter"
print "c := [mitte[0]+norm_ortho[0]*hoehe,mitte[1]+norm_ortho[1]*hoehe], wobei \"mitte\" der Punkt zwischen \"b\" und \"d\" ist und auf \"vec\" liegt"
print "     und \"norm_ortho\" der normalisierte Orthogonal-Vektor auf \"vec\" ist und \"hoehe\" der Abstand zwischen \"mitte\" und \"c\""
print "=============================================================="
rek=raw_input("Wie viele Rekursions-Schritte sollen berechnet werden?:")
print "Rechne..."
kochSnowFlake(float(rek))
print "Fertig :)"
zeichnen(globArray)

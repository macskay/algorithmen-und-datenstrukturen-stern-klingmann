# -*- coding: cp1252 -*-
#Klingmann, Stern, Schönpflug
#Aufgabe 3b und c

liste=[0, 1, 2, 3, 4, 5, 6, 8, 18, 22, 23, 41, 48, 55, 59, 66, 77, 78, 81, 84, 321, 521, 564, 999, 1333, 55678]

################# AUFGABENTEIL 3b #################
def binarySearchRef(a, key, start, end):
    size = end - start
    if size <= 0:
        return None
    center = (start + end)/2
    if key == a[center]:
        return center
    elif key < a[center]:
        return binarySearchRef(a, key, start, center)
    else:
        return binarySearchRef(a, key, center+1, end)

def binarySearchValue(a, key):
    if len(a) == 0:
        return None
    center = len(a)/2
    if key == a[center]:
        return center
    elif key < a[center]:
        return binarySearchValue(a[:center], key)
    else:
        res = binarySearchValue(a[center+1:], key)
        if res is None:
            return None
        else:
            return res + center + 1

################# AUFGABENTEIL 3c #################
def binarySearchIter(array, key):
    ende = len(array)-1
    index=0
    while index <= ende:        #solange wir nicht am Ende sind
        center = index + ((ende - index)/2)
        if array[center] == key:
            return center
        elif key < array[center]:
            ende = center - 1
        else:
            index = center + 1
    return "Der Key wurde nicht gefunden"
	
################# TESTBEREICH #################
if __name__ == "__main__":
    #Test Aufgabenteil b
    from timeit import Timer
    print "Das Array:",liste
    print "Gesucht wird nach 521"
    t1 = Timer('binarySearchRef(array, 521, 0, len(array)-1)',"from __main__ import liste, binarySearchRef; array=liste")
    print "Per Referenzübergabe dauert das Suchen:", t1.timeit() ,"Sekunden"
    t2 = Timer('binarySearchValue(array, 521)',"from __main__ import liste, binarySearchValue; array=liste")
    print "Per Wertübergabe dauert das Suchen:",t2.timeit(),"Sekunden"
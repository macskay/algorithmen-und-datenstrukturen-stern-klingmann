# -*- coding: cp1252 -*-
#Klingmann, Stern, Schönpflug
import unittest
from BinarySearch import binarySearchIter, binarySearchRef

class MeinBSTest(unittest.TestCase):
    def setUp(self):
        self.liste=[0, 1, 2, 3, 4, 5, 6, 8, 18, 22, 23, 41, 48, 55, 59, 66, 77, 78, 81, 84, 321, 521, 564, 999, 1333, 55678]

    def test_binarySearchIter(self):
        print self.liste
        self.assertEqual(binarySearchIter(self.liste, 521),23) # Verifizierung durch Hardcode
        self.assertEqual(binarySearchIter(self.liste, 521),binarySearchRef(self.liste, 521, 0, len(self.liste)-1)) #Verifizierung durch Test mit rekursiver Funktion
        
if __name__ == "__main__":
    unittest.main()

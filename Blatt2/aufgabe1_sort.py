# -*- coding: cp1252 -*-
import random
import copy
import time


#Liste zum testen
k=10  #Array-Gr��e
n=100 #Anzahl Durchg�nge
liste1 = []
for rand in range(k):
    rand = random.randint(1,k)
    liste1.append(rand)

liste2 = copy.deepcopy(liste1)
liste3 = copy.deepcopy(liste1)


#Selection Sort
def selectionSort(a):
    comparisonCount = 0
    for i in range(len(a)-1):
        min = i
        for j in range (i+1, len(a)):
            comparisonCount += 1
            if a[j] < a[min]:
                min = j
        a[i], a[min] = a[min], a[i]         #swap
    return a, comparisonCount
        
#Merge Sort
def mergeSort(a, mergeCount):
    if len(a) <= 1:
        return a,  mergeCount  
    else:
        left  = a[:len(a)/2]   
        right = a[len(a)/2:]   
        leftSorted,  mergeCount  = mergeSort(left, mergeCount)  
        rightSorted,  mergeCount = mergeSort(right, mergeCount) 
        return merge(leftSorted, rightSorted, mergeCount)
    
def merge(a,b,mergeCount):
    c = []    
    i, j = 0, 0
    while i < len(a) and j < len(b):
        mergeCount += 1
        if a[i] <= b[j]:
             c.append(a[i])
             i += 1
        else:
             c.append(b[j])
             j += 1
    if i < len(a):
       c += a[i:]
    else:
       c += b[j:]
    return c, mergeCount
    
#Quick Sort
def quickSort(a, l, r, comparisonCount): 
    if r > l:                  
         i, comparisonCount = partition(a, l, r, comparisonCount)    
         a, comparisonCount = quickSort(a, l, i-1, comparisonCount)     
         a, comparisonCount = quickSort(a, i+1, r, comparisonCount)
    return a, comparisonCount

def partition(a, l, r, comparisonCount):
    pivot = a[r]     
    i  = l           
    j  = r - 1

    while True:
        comparisonCount += 1
        while i < r and a[i] <= pivot:
            comparisonCount += 1
            i += 1
        comparisonCount += 1
        while j > l and a[j] >= pivot:
            comparisonCount += 1
            j -= 1               
        if i >= j:               
            break                      
        a[i], a[j] = a[j], a[i]
    comparisonCount += 1
    if a[i] > pivot:
        a[i], a[r] = a[r], a[i]      
                                
    return i, comparisonCount                   

def checkSorting(arrayBefore, arrayAfter):
    sortiert = True

    #Auf L�nge pr�fen
    if len(arrayBefore) != len(arrayAfter):
        sortiert = False

    #Auf Sortiertheit pr�fen bzgl der Werte
    for i in range(len(arrayBefore)-1):
        if not len(arrayAfter) <= 1:
            for i in range(len(arrayAfter)-1):
                if arrayAfter[i] > arrayAfter[i+1]:
                    sortiert = False
            
    #Auf gleiche Elemente pr�fen
    for i in range(len(arrayBefore)):
        elemCountB=0
        elemCountA=0
        tempElem=arrayBefore[i]
        for j in range(len(arrayBefore)):
            if tempElem == arrayBefore[j]:
                elemCountB +=1
        for p in range(len(arrayAfter)):
            if tempElem == arrayAfter[p]:
                elemCountA +=1
        if elemCountB != elemCountA:
            sortiert = False
            break

    return sortiert

    
#Eingabe und Ausgabe
templiste = copy.deepcopy(liste1)

strich = "---"

comparisonCount1 = 0
startzeit = time.time()

for i in range(n):
    liste1, comparisonCount1 = selectionSort(liste1)
    if i == n-1:
        print "Liste 1 sortiert mit selection sort:"
        #print liste1
    liste1 = copy.deepcopy(templiste)

endzeit = time.time()
endzeit -= startzeit
print "Count fuer einen Durchgang:",comparisonCount1, "| Time:", endzeit/n
print 15 * strich

#----------------------------------------------------------------------------------------------------

templiste = copy.deepcopy(liste2)
comparisonCount2 = 0
startzeit = time.time()
for i in range(n):
    comparisonCount2 = 0
    liste2, comparisonCount2 = mergeSort(liste2, comparisonCount2)                     
    if i == n-1:
        print "Liste 2 sortiert mit merge sort:"
        #print liste2
    liste2 = copy.deepcopy(templiste)
    
endzeit = time.time()
endzeit -= startzeit
print "Count fuer einen Durchgang:",comparisonCount2, "| Time:", endzeit/n
print 15 * strich





#----------------------------------------------------------------------------------------------------

templiste = copy.deepcopy(liste3)
len3 = len(liste3)
comparisonCount3 = 0
startzeit = time.time()
for i in range(n):
    comparisonCount3 = 0
    liste3, comparisonCount3 = quickSort(liste3,0,len3-1, comparisonCount3)                    
    if i == n-1:
        print "Liste 3 sortiert mit quick sort:"
        #print liste3
    liste3 = copy.deepcopy(templiste)

endzeit = time.time()
endzeit -= startzeit
print "Count fuer einen Durchgang:",comparisonCount2, "| Time:", endzeit/n
print 15 * strich

print "Test-Listen werden erstellt, welche falsch gemischt sind"
testlisteBefore = [3,2,3,1]
testlisteAfter = [1,1,2,3]
print "checkSorting �berpr�ft:"
print checkSorting(testlisteBefore,testlisteAfter)


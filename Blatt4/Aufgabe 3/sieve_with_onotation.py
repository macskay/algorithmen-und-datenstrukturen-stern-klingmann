# -*- coding: cp1252 -*-
#Klingmann, Stern
import math
def sieb(n):
    P = range(2,n+1)                #O(1)   -|       
    i = 0                           #O(1)   -|     
    p = P[i]                        #O(1)   -|                                                                                                                                                                            
    counti=0                        #O(1)   -|---> O(1) nach Sequenz                                                                                                                                                 -|
    counto=0                        #O(1)   -|                                                                                                                                                                        |
    print "Rechne..."               #O(1)   -|                                                                                                                                                                        |                                
    summe=0                         #o(1)   -|                                                                                                                                                                        |
    while p*p <= n:                 #O((n-p)/p)                                                                                                                                        -|                             |   
        counto+=1                       #O(1)  -| --> O(1) nach Sequenz                                                                                -|                               |                             |
        v = 2*p                         #O(1)  -|                                                                                                       |                               |                             |                          
        while v <= P[-1]:               #O(n-2 - v)                                                            -|                                       |                               |--> O((n-p)/p)*((n-2-v)*n)) -|
            if v in P:                      #O(n)       -|                                                      |---> O((n-2 - v) * n) nach Schacht.   -|                               |   nach Schachtelung         |---> O((n-p)/p)*(n-2-v*n))
                counti+=1                       #O(1)   -|---> O(n) nach Schachtelung -|                        |                                       |                               |                             |     nach Sequenz
                P.remove(v)                     #O(1)   -|                             |---> O(n) nach Sequenz -|                                       |----> O(n-2-v*n) nach Sequenz -|                             |
            v = v+p                         #O(1)                                     -|                                                                |                                                             |
        i = i+1                         #O(1) -|                                                                                                                                                                      |                                                                                                        |
        print "Primzahl:",p             #O(1) -|                                                                                                                                                                      |                                                                                                        |
        temp=float(p)                   #O(1) -| ---> O(1) nach Sequenz                                                                                                                                               |                                                                               -|
        summe+=1/temp                   #O(1) -|                                                                                                                                                                      |                                                                                                            
        p = P[i]                    #O(1)  -|                                                                                                                                                                         |
    print "EODE"                    #O(1)  -|                                                                                                                                                                         |
    print "Counto:",counto          #O(1)  -|                                                                                                                                                                         |
    print "Counti:",counti          #O(1)  -|---> O(1) nach Sequenz                                                                                                                                                  -|
                                    #       |
    komp=n*math.log(math.log(n))    #O(1)  -|
    print "Komp:",komp              #O(1)  -|
    return P,summe                  #O(1)  -|


x=1000
liste,summe = sieb(x)
print "Summe �ber p:",summe
print
print "Die Komplexit�t �ber die Regeln der O-Notation lautet: O(((n-p)/p)*((n-2-v)*n))"

def lhospital(summe,n):
    print "Die Berechnung �ber l'Hospital hat ergeben:\nDer Sieb des Erathosthenes"
    g=n*math.log(math.log(n))
    h=summe
    stringTemp=""
    if summe/g<=1:
        stringTemp="liegt"
    else:
        stringTemp="liegt nicht"
    print stringTemp,"in O(n*log(log(N)), denn "
    print "(",summe,"/",g,")","=",summe/n,"<=",1

print
print
lhospital(summe,x)
    



    

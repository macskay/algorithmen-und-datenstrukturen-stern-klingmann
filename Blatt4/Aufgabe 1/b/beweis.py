# -*- coding: cp1252 -*-
#Julien Stern & Max Klingmann
#Beweis der Ordnung folgender Funktionen nach aufsteigender Komplexi�t mit
#Hilfe der Regel von l'Hospital

from math import sqrt, log

#Nachfolgend meine Reihenfolge der Funktionen nach Definierung der Python-Funktion
#f1 = f, f2 = g, f3 = h, f4 = i, f5 = j, f6 = k
def f(x):
    return sqrt(log(x, 2))

def g(x):
    return log(x, 2)

def h(x):
    return sqrt(x)

def i(x):
    return x**3

def j(x):
    return 3**x

def k(x):
    return x**x 


#Die �berpr�fung nach l'Hospital
def lhospital():
    for x in range(100,200,10):
        '''Simulation der Limes-Rechnung aber anstatt gegen Unendlich sind gro�e N genutzt worden.
        Gr��ere N liefern aber bei x**x zu gro�e Zahlen.
        Die Grafiken im Anhang veranschaulichen aber, dass bereits ab Werten von N>=100 ausreichende Ergebnis gebracht werden, um den Beweis zu erbringen'''
        c=5
        if f(x)/(c*g(x))>1:
            return False,1
        c-=1
        if g(x)/(c*h(x))>1:
            return False,2
        c-=1
        if h(x)/(c*i(x))>1:
            return False,3
        c-=1
        if i(x)/(c*j(x))>1:
            return False,4
        c-=1
        if j(x)/(c*k(x))>1:
            return False,5
        print "Ergebnis f�r x =",x
        print f(x),"<",g(x),"<",h(x),"<",i(x),"<",j(x),"<",k(x)
        print
    return True,0


#Etwaige Fehlererkennung
antwort,code=lhospital()
if(antwort):
    print "Alles glatt gelaufen und jede Funktion hat ein Ergebnis <= 1 zur�ckgeliefert"
else:
    if code==0:
        print "Alles glatt gelaufen!" #Fall darf nie auftreten!
    elif code==1:
        print "Fehler bei �berpr�fung 1: f(x) & g(x)"
    elif code==2:
        print "Fehler bei �berpr�fung 1: g(x) & h(x)"
    elif code==3:
        print "Fehler bei �berpr�fung 1: h(x) & i(x)"
    elif code==4:
        print "Fehler bei �berpr�fung 1: i(x) & j(x)"
    elif code==5:
        print "Fehler bei �berpr�fung 1: j(x) & k(x)"
    else:
        print "Also Das muss v�llig verkehrt sein" #Wenn das passiert hat man v�lligen Mist gebaut
     

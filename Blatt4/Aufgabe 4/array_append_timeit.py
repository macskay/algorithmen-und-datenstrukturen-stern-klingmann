# -*- coding: cp1252 -*-
#Klingmann, Stern
import timeit
array1=[]            # leere Liste erzeugt


# F�hrt append() einmal aus
def a():        
    array1.append(0)

# Initialisierung der Time-Objekte t_a und t_exp      
t_a=timeit.Timer("a()","from __main__ import a")

#Ausgabe
n=10

for j in range(n):
    print "Die",j,"ten",n," Durchlaeufe ergaben:"
    mittel=0
    for i in range(1,n+1):
        del array1[:]
        i_help=i*100000
        t=t_a.timeit(i_help)/i_help
        mittel +=t
    print "Mittelwert:",mittel
    print "----------"
print "==> Das Python Array \"list\" ist ein dynamisches."

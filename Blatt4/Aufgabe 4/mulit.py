#Klingmann,Stern
import timeit
import copy
size=3
size_hoch2=size*size
A=range(size_hoch2)
B=range(size_hoch2)
C1=[0]*size_hoch2
C2=copy.deepcopy(C1)

def multi_alt():
    for i in range(size):
        for j in range(size):
            for k in range(size):
                C1[i+j*size]+=A[i+k*size]*B[k+j*size]

def multi_neu():
    for i in range(size):
        
        for j in range(size):
            j_temp=j*size
            x=i+j_temp
            for k in range(size):
                C2[x]+=A[i+k*size]*B[k+j_temp]
            
    
    

t_alt=timeit.Timer("multi_alt()","from __main__ import multi_alt")
t_neu=timeit.Timer("multi_neu()","from __main__ import multi_neu")

n=10
alt=0
neu=0

for i in range(n):
    alt+=t_alt.timeit(1000)
    neu+=t_neu.timeit(1000)

print "Mulit_alt braucht im Schnitt:",alt/n,"sek"
print "----------------"
print "Mulit_neu braucht im Schnitt:",neu/n,"sek"
    
print C1
print C2



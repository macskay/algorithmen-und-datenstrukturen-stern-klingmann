# -*- coding: cp1252 -*-
#Klingmann, Stern, Sch�npflug
#Aufgabenblatt 10, Aufgabe 2

import json
import sys
import operator
import heapq
import math

PI = 3.14159265

def openJsonFile(datei):
	try:
		return json.load(open(datei))
	except:
		print "Datei wurde nicht gefunden!"
		sys.exit(0)

###################################################### AUFGABENTEIL 2 ######################################################

def nachbarnAusgeben(dictionary):
        sortedStaedte = staedte.keys()
        sortedStaedte.sort()
        adjazenzListe=[]
        for key in sortedStaedte:
                nachbarListe=[]
                for nachbarn in dictionary[key]["Nachbarn"]:
                        tempListe=()
                        #print nachbarn,"-",dictionary[key]["Nachbarn"][nachbarn]
                        tempListe = sortedStaedte.index(nachbarn), dictionary[key]["Nachbarn"][nachbarn]
                        nachbarListe.append(tempListe)
                adjazenzListe.append(nachbarListe)
        return adjazenzListe, sortedStaedte
                      
def dijkstra(graph, start, ziel):	# graph: gewichtete Adjazenzliste
        heap = []
        visited = [None]*len(graph)
        visited[start] = start
        for neighbor in graph[start]:
                heapq.heappush(heap, (neighbor[1], start, neighbor[0])) # neighbor[1]:Kantengewicht,neighbor[0]:Endpunkt d. K.
        while len(heap) > 0:	# solange der heap nicht leer ist
                w, fromNode, node = heapq.heappop(heap)
                if visited[node] is not None:	# wenn der k�rzeste Pfad bereits bekannt ist, �berspringe ihn
                       continue
                visited[node] = fromNode    # baue Vorg�nger-Baum
                if node == ziel:	# da der heap noch nicht leer ist, wird an dieser Stelle ein break ben�tigt
                       break
                for neighbor in graph[node]:
                        if visited[neighbor[0]] is not None:	# wenn der k�rzeste Pfad bereits bekannt ist, �berspringe ihn
                                continue
                        heapq.heappush(heap, (neighbor[1]+w, node, neighbor[0]))
        bestPath = []
        t = ziel
        while t != visited[t]:		# Array wird durchlaufen bis der Anker des Pfades gefunden ist, vgl. Union-Search
                bestPath.append(t)
                t=visited[t]
        bestPath.append(start)
        return bestPath			# bestPath.reverse()

def entf(graph, bestPath, sortStadt):
        dist=0
        bestPath=bestPath[::-1]
        for stadt in range(len(bestPath)):
                if bestPath[stadt] == bestPath[-1]:
                        break
                dist+=graph[sortStadt[bestPath[stadt]]]["Nachbarn"][sortStadt[bestPath[stadt+1]]]
        return dist

def aufgabe2(staedte):
        print "Aufgabe 2:"
        print "----------"
        
        adjLt, sortStadt = nachbarnAusgeben(staedte)
        start=raw_input("Bitte eine Anfangsstadt eingeben (Case-Sensitive):")
        ende=raw_input("Bitte eine Zielstadt einegeben (Case-Sensitive):")
        bestPath=dijkstra(adjLt, sortStadt.index(start), sortStadt.index(ende))
        print "Von",start,"nach",ende,":"
        for elem in bestPath[::-1]:
                print "  ",sortStadt[elem]
        print "Die Entfernung betr�gt:"
        print "  ",entf(staedte, bestPath, sortStadt),"Kilometer"
        print "Sie haben Ihr Ziel erreicht!"
        return entf, start, ende
        

###################################################### AUFGABENTEIL 3 ######################################################

def luftlinie(dictionary, start, ziel):
        koordStart=[]
        koordZiel=[]
        for koord in dictionary[start]["Koordinaten"]:
                koordStart.append(dictionary[start]["Koordinaten"][koord])
        for koord in dictionary[ziel]["Koordinaten"]:
                koordZiel.append(dictionary[ziel]["Koordinaten"][koord])

        startLen = float(koordStart[1][:2]) + (float(koordStart[1][3:]) / 60)
        startBre = float(koordStart[0][:2]) + (float(koordStart[0][3:]) / 60)
        zielLen = float(koordZiel[1][:2]) + (float(koordZiel[1][3:]) / 60)
        zielBre = float(koordZiel[0][:2]) + (float(koordZiel[0][3:]) / 60)
        
        startLenDeg = startLen / 180 * PI
        startBreDeg = startBre / 180 * PI
        zielLenDeg = zielLen / 180 * PI
        zielBreDeg = zielBre / 180 * PI
       
        erg = math.acos(math.sin(startBreDeg)*math.sin(zielBreDeg) + math.cos(startBreDeg)*math.cos(zielBreDeg)*math.cos(zielLenDeg-startLenDeg))
        entf = erg * 6378.137
        return entf

        
def aufgabe3(staedte, stadt):
        print "Aufgabe 3:"
        print "----------"
        
        adjLt, sortStadt = nachbarnAusgeben(staedte)

        eingabe=int(input("F�r Aufgabe 3 die gleichen St�dte, wie bei Aufgabe 2 �bernehmen? [1]=Ja, [0]=Nein: "))
        if eingabe==0:                 
                start=raw_input("Bitte eine Anfangsstadt eingeben (Case-Sensitive):")
                ende=raw_input("Bitte eine Zielstadt einegeben (Case-Sensitive):")
        else:
                start=stadt[1]
                ende=stadt[2]

        print "Die Luftlinie zwischen",start,"und",ende,"betr�gt:"
        print "   ",luftlinie(staedte, start, ende),"Kilometer"


###################################################### TESTBEREICH ######################################################
#####
# F�r die Aufgaben 2a) - 2d) bitte einfach die entsprechenden Ortschaften eingeben und man bekommt das Ergebnis geliefert
# Bei Aufgabe 3 bitte analog verfahren
# Der Test ergibt sich aus dem Ausprobieren der beiden Methoden. Man sieht, dass die Luftlinienrechnung immer kleiner ist als die Strassenrechnung
#####


if __name__ == "__main__":
        staedte = openJsonFile('entfernungen.json')
        
        stadt=aufgabe2(staedte)
        print
        aufgabe3(staedte, stadt)
        
        

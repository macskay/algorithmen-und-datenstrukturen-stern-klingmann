#-*- coding: iso-8859-1 -*-
#Klingmann, Sch�npflug, Stern

import json
import random

def openJsonFile(datei):
    try:
        return json.load(open(datei))
    except:
        print "Datei wurde nicht gefunden!"
        sys.exit(0)
        

def acyclic(graph):
        def visit(graph, node, fromNode, visited):
            if visited[node]:			# Zyklus entdeckt
                return False
            visited[node] = True
            for neighbor in graph[node]:
                if neighbor == fromNode:	# �berspringe Nachbar, von dem du gekommen bist
                    continue
                if not visit(graph, neighbor, node, visited):
                    return False		# der Graph ist zyklisch
            return True			# kein Zyklus
        visited = [False]*len(graph)
        for node in range(len(graph)):
            if visited[node]:	# schlie�t aus, dass Knoten besucht wird, der schon besucht war
                continue
            if not visit(graph, node, None, visited):
                return False
        return True
    
def make_table():
    T = {}
    for i in range(26):
        T[chr(i+97)] = randomArray()
    T[u'\xc4'] = randomArray()
    T[u'\xd6'] = randomArray()
    T[u'\xdc'] = randomArray()
    T[u'\xe4'] = randomArray()
    T[u'\xf6'] = randomArray()
    T[u'\xfc'] = randomArray()
    T[u'\xdf'] = randomArray()   
    return T

def randomArray():
    random.seed()
    array =[]
    for i in range(10):
        array.append(random.randint(0,10000)%154)
    return array

def search_table(table,pos,char):
    lowchar = char.lower()
    return table[lowchar][pos]

def mapping(dictionary):
    print "Funktion generiert leider sehr oft"
    print "neue T1 und T2, da acyclic(graph) immer"
    print "false zur�ck gibt"
    print "Funktion rechnet..."
    while True:
        T1 = make_table()
        T2 = make_table()
        graph=[]
        for element in dictionary:
            sum1 = 0
            sum2 = 0
            if len(element) > 5:
                b=5
            else:
                b=len(element)
            for i in range(b):
                sum1 += search_table(T1,i,element[i])
                sum2 += search_table(T2,i,element[i])
            sum1 = sum1%154
            sum2 = sum2%154
            edge=[sum1,sum2]
            graph.append(edge)
        if acyclic(graph):
            break
    print graph
    
#def perfect_hash(city):

staedte = openJsonFile('entfernungen.json')
mapping(staedte)

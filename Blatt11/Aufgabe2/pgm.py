def readPGM(filename):
    s = file(filename, 'rb').read()
    n1 = s.find('\n')
    n2 = s.find('\n', n1+1)
    n3 = s.find('\n', n2+1)

    # check
    if s[:n1] != 'P5':
        raise ValueError("'%s' is not a valid PGM file")
    w, h, c = [int(k) for k in s[n2+1:n3].split()]
    data = [ord(k) for k in s[n3+1:]]
    return w, h, data
    
def writePGM(width, height, data, filename):
    s = 'P5\n# width height max_col\n%d %d %d\n' % (width, height, 255)
    t = ''.join([chr(k) for k in data])
    # import array
    # t = array.array('B', data).tostring()
    file(filename, 'wb').write(s + t)

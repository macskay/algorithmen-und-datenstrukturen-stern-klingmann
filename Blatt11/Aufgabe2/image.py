import copy
from pgm import readPGM, writePGM

def createMask(width, heigth, data, threshold): # Aufgabe 2a
    mask = copy.copy(data)
    for x in range(width):
        for y in range(heigth):
            if data[x+y*width] < threshold:
                mask[x+y*width] = 0
            else:
                mask[x+y*width] = 255
    return mask

def createGraph(width, height, mask): # Aufgabe 2b
    graph = []
    for x in range(width):
        for y in range(height):
            knoten = []
            value = mask[x+y*width]
            if x-1 > 0 and value == mask[(x-1)+y*width]:
                knoten.append((x-1)+y*width)
            if x+1 < width and value == mask[(x+1)+y*width]:
                knoten.append((x+1)+y*width)
            if y-1 > 0 and value == mask[x+(y-1)*width]:
                knoten.append(x+(y-1)*width)
            if y+1 < heigth and value == mask[x+(y+1)*width]:
                knoten.append(x+(y+1)*width)
            graph.append(knoten)
    return graph

def connectedComponents(graph): # Aufgabe 2c
       def findAnchor(anchors, k):
               while anchors[k] != k:
                       k = anchors[k]
               return k
       def edges(graph):
               e = []
               for node in range(len(graph)):
                       for n in graph[node]:
                               if node < n:
                                       e.append((node, n))
               return e
       anchors = range(len(graph))
       for edge in edges(graph):
               a1, a2 = findAnchor(anchors, edge[0]), findAnchor(anchors, edge[1])
               if a2 < a1: a2,a1 = a1,a2
               if a1 != a2: anchors[a2] = a1
       for node in range(len(graph)):
               anchors[node] = findAnchor(anchors, node)
       return anchors

#def anchorsToLabels(anchors): # Aufgabe 2d

    

width, heigth, data = readPGM('cells.pgm')
mask = createMask(width, heigth, data, 60)
writePGM(width, heigth, mask, 'mask.pgm')
graph = createGraph(width, heigth, mask)
anchors = connectedComponents(graph)

f = open("graph.txt", "wb")
for item in graph:
    f.write("%s\n" % item)
f.close()

g = open("anchors.txt", "wb")
for item in anchors:
    g.write("%s\n" % item)
g.close()

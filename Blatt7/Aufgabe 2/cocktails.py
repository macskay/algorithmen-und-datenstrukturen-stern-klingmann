#-*- coding: iso-8859-1 -*-
#Klingmann, Sch�npflug, Stern

import json
import sys
import operator


def openJsonFile(datei):
    try:
        return json.load(open(datei))
    except:
        print "Datei wurde nicht gefunden!"
        sys.exit(0)
        
def removeBrackets(string):
    for bs in range(len(string)):
        if string[bs] == "(":
            if string[bs-1] == " ":
                return string[:bs-1]
            return string[:bs]
        elif string[bs] == ",":
            return string[:bs]
        elif string[-1] == " ":
            return string [:-1]
    return string

def allIngredients(dictionary):
    for element in dictionary:
        for ingr in dictionary[element]["ingredients"]:
            ingr=ingr.lower()
            ingr=removeBrackets(ingr)
            if ingr not in zutatenListe:
                zutatenListe.append(ingr)
                prioListe[ingr] = 1                
            else:
                prioListe[ingr] += 1

def zaehleZutaten(dictionary):
    anzahl=0
    for elem in range(len(dictionary)):
        anzahl = elem
    return anzahl
        

def highscore(dictionary):
    sortedPrioList = sorted(dictionary.iteritems(), key=operator.itemgetter(1))
    sortedPrioList.reverse()

    print "Die Zutaten mit den meisten Anwendungen sind:"
    for elem in range(15):
        print sortedPrioList[elem][1], " - ", sortedPrioList[elem][0]

zutatenListe=[]
prioListe={}
inverseRecipes={}
recipes = openJsonFile('cocktails.json')

allIngredients(recipes)
zutatenListe.sort()
#print "Anzahl der Zutaten:",zaehleZutaten(prioListe)

#Aufgabenteil b)
#-------------------------------------------------------------------------------------------

def cocktailsInverse(dictionary):
    inverseDict={}
    for elem in zutatenListe:
        inverseDict[elem]=[]
        for cockt in dictionary:
            for ingr in dictionary[cockt]["ingredients"]:
                ingr=ingr.lower()
                pos = ingr.find(elem)
                if pos!=-1:
                    inverseDict[elem].append(cockt)
    return inverseDict
                
inverseRecipes = cocktailsInverse(recipes)

def dateiErzeugen():
    try:
        datei=open("cocktails_inverse.json","w")
        return datei
    except:
        print "Dateizugriff fehlgeschlagen"
        sys.exit(0)

def dateiBeschreiben(dateiname, jsonDump):
    try:
        dateiname.write(jsonDump)
    except:
        print "Beschreiben fehlgeschlagen!"
        
d = dateiErzeugen()
jD = json.dumps(inverseRecipes, sort_keys=True, indent=4)
dateiBeschreiben(d, jD)
d.close()


#Aufgabenteil c)
#---------------------------------------------------------------------------

zutatenDatei = openJsonFile('cocktails_inverse.json')
availableIngredients = ["mett","zwiebel","salz","pfeffer","genever"]

def possibleCocktails(zutatenDatei, availableIngredients):
    setList=[]
    for ingr in availableIngredients:
        tempSet=set(zutatenDatei[ingr])
        setList.append(tempSet)
    for elem in range(len(setList)):
        if elem==0:
            tempSet=setList[elem]
        else:
            tempSet=setList[elem] & tempSet
    print "Mit den folgenden Ingredientien "
    for ingr in availableIngredients:
        print " ",ingr
    print "kann man diese Cocktails mixen:"
    for elem in tempSet:
        print " ",elem

        
possibleCocktails(zutatenDatei, availableIngredients)

# Wir erdachten keine ignoreList, da es �u�erst subjektiv behaftet, welch Ingredient als Dekoration g�lte.
# :likeasir:


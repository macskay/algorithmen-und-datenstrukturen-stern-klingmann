# -*- coding: cp1252 -*-
#Klingmann, Stern
import unittest
from Calculator import *

class MeinTest(unittest.TestCase):
    def setUp(self):
        del stack[:]
        
    def test_op_teilen(self):
        tempstr="2+4"
        temp=op_teilen("-","+",tempstr)
        self.assertEqual(temp,"2")

        tempstr="2*4"
        temp=op_teilen("*","*",tempstr)
        self.assertEqual(temp,"2")

    def test_teilen(self):
        tempstr="2+3"
        deinemuddaihrarray=teilen(tempstr)
        self.assertEqual(deinemuddaihrarray[0],"+")

    def test_an_fuellen(self):
        del an[:]
        arrayZ=["+","2","3"]
        an_fuellen(arrayZ)
        self.assertEqual(an[0].key, arrayZ[0])
        
        
    def test_arrayParsen(self):
        tempArray=[Node("2"),Node("+"),Node("4")]
        arrayParsen(tempArray)
        self.assertEqual(type(tempArray[0].key),type(1))
        self.assertNotEqual(type(tempArray[1].key),type(1))    
        self.assertEqual(type(tempArray[2].key),type(1))

    def test_arraySplitten(self):
        tempArray=[Node("-"),Node("*"),Node(1),Node(4),Node("+"),Node(2),Node(7)]
        left=[Node("*"),Node(1),Node(4)]
        right=[Node("+"),Node(2),Node(7)]
        templ,tempr = arraySplitten(tempArray)
        self.assertEqual(templ[0].key,left[0].key)
        self.assertEqual(tempr[2].key,right[2].key)

    def test_baumVerknuepfen(self):
        right=[Node("+"),Node(2),Node(7)]
        baumVerknuepfen(right)
        self.assertEqual(right[1].key,right[0].left.key)
        self.assertEqual(right[2].key,right[0].right.key)

    def test_calc(self):
        arrayAdd=[Node("+"),Node(2),Node(7)]
        baumVerknuepfen(arrayAdd)
        summ=calc(arrayAdd[0])
        self.assertEqual(summ,9)

        arrayDiff=[Node("-"),Node(2),Node(7)]
        baumVerknuepfen(arrayDiff)
        diff=calc(arrayDiff[0])
        self.assertEqual(diff,-5)

        arrayProd=[Node("*"),Node(2),Node(7)]
        baumVerknuepfen(arrayProd)
        prod=calc(arrayProd[0])
        self.assertEqual(prod,14)
        
        arrayQuo=[Node("/"),Node(4),Node(2)]
        baumVerknuepfen(arrayQuo)
        quo=calc(arrayQuo[0])
        self.assertEqual(quo,2)
    
        
if __name__ == '__main__':
    unittest.main()

# -*- coding: cp1252 -*-
#Klingmann, Stern
rechnung="2*4*(3+(4-7)*8)-(1-6)"

stack=[]
def teilen(string):
    kZaehler=0              #Hilfz�hler der bei der "Schneid-Entscheidung" hilft
    schneiden=True          #True, wenn Klammern an den Enden zusammengeh�ren
    subStrA=""              #Kind A
    subStrB=""              #Kind B
    
#-----------------------------------------------------------------------------------------
#HIER WIRD GEPR�FT, OB STRING MIT "(" BEGINNT UND MIT ")" ENDET UND DIESE ZUSAMMENGEH�REN
#FALLS DIES DER FALL IST, SO KANN GESCHNITTEN WERDEN (schneiden == True )

    if string[0] == "(" and string[len(string)-1] == ")":
        #An beiden Enden Klammern erkannt
        for i in range(1,len(string)-1):
            if string[i] == "(":
                kZaehler +=1
            if string [i] ==  ")":
                kZaehler -=1
                if kZaehler < 0:
                    schneiden = False
                    break
        if schneiden:
            string = string[1:-1]


#HIER WIRD AUF "+" ODER "-" GEPR�FT

    tempStr= op_teilen("+","-",string)


#HIER WIRD GEPR�FT, OB subStrA == "". IST DIES DER FALL, SO HAT DER ALOG BISHER NICHT GETEILT
    
    if tempStr == "":
        op_teilen("*","/",string)
    
    return stack
#-----------------------------------------------------------------------------------------       

def op_teilen(op1,op2,string):
    ka=0
    ke=0
    k=False                                              
    subStrA=""              
    subStrB=""              
    knoten=""                 
    z=1
    for i in string:        
        if i == "(":
            ka +=1
            k = True
                
        if i == ")":
            ke +=1

        if ka !=0 and ka == ke:
            k = False

        if i == op1 or i == op2:
            if k == False:
                knoten = i
                subStrA = string[:z-1]
                subStrB = string[z:]
                stack.append(knoten)
                if len(subStrA) == 1:
                    stack.append(subStrA)
                if len(subStrB) == 1:
                    stack.append(subStrB)
                teilen(subStrA)
                teilen(subStrB)
                break
            else:
                z +=1
                continue
        z +=1
    return subStrA      

teilen(rechnung)

an=[]


class Node:
    def __init__(self, key):
        self.key = key
        self.left = None
        self.right = None


def an_fuellen(array):
    for i in array:
        p = Node(i)
        an.append(p)
          

def arrayParsen(array):
    for node in array:
        try:
            p=int(node.key)
            node.key = p
        except:
            continue


def arraySplitten(array):
    index=1
    while type(array[index].key) is not type(1) or type(array[index+1].key) is not type(1):
        index += 1
            
    arrayLeft = array[1:index+2]
    arrayRight = array[index+2:]
    return arrayLeft, arrayRight


def baumVerknuepfen(baumArray):
    i=0
    while i+2 < len(baumArray):
        baumArray[i].left = baumArray[i+1]
        baumArray[i].right = baumArray[i+2]
        
        print "Wurzel:",baumArray[i].key
        print "Linkes Kind:",baumArray[i].left.key
        print "Rechtes Kind:",baumArray[i].right.key
        print
        
        i+=2   

an_fuellen(stack)   
arrayParsen(an)
arrayLeft,arrayRight = arraySplitten(an)
root = an[0]
root.left = arrayLeft[0]
root.right = arrayRight[0]

print "Grund-Wurzel:",root.key
print "Linkes Kind:",root.left.key
print "Rechtes Kind:", root.right.key
print

baumVerknuepfen(arrayLeft)
baumVerknuepfen(arrayRight)

    
def calc(node):
    if node.key == "+":
        if type(node.left.key) is type(1) and type(node.right.key) is type(1):
            return node.left.key + node.right.key
        elif type(node.left.key) is not type(1) and type(node.right.key) is type(1):
            return calc(node.left) + node.right.key
        elif type(node.right.key) is not type(1) and type(node.left.key) is type(1):
            return node.left.key + calc(node.right)
        else:
            return calc(node.left) + calc(node.right)

    elif node.key == "-":
        if type(node.left.key) is type(1) and type(node.right.key) is type(1):
            return node.left.key - node.right.key
        elif type(node.left.key) is not type(1) and type(node.right.key) is type(1):
            return calc(node.left) - node.right.key
        elif type(node.right.key) is not type(1) and type(node.left.key) is type(1):
            return node.left.key - calc(node.right)
        else:
            return calc(node.left) - calc(node.right)    

    elif node.key == "*":
        if type(node.left.key) is type(1) and type(node.right.key) is type(1):
            return node.left.key * node.right.key
        elif type(node.left.key) is not type(1) and type(node.right.key) is type(1):
            return calc(node.left) * node.right.key
        elif type(node.right.key) is not type(1) and type(node.left.key) is type(1):
            return node.left.key * calc(node.right)
        else:
            return calc(node.left) * calc(node.right)

    elif node.key == "/":
        if type(node.left.key) is type(1) and type(node.right.key) is type(1):
            return node.left.key / node.right.key
        elif type(node.left.key) is not type(1) and type(node.right.key) is type(1):
            return calc(node.left) / node.right.key
        elif type(node.right.key) is not type(1) and type(node.left.key) is type(1):
            return node.left.key / calc(node.right)
        else:
            return calc(node.left) / calc(node.right)

print "Berechne Baum:",calc(root)
     


  
    







            





        


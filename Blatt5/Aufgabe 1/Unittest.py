# -*- coding: cp1252 -*-
import unittest
from Node import *

class TestNodeFunctions(unittest.TestCase):
    def setUp(self):
        self.n=Node(5)

    def test_treeInsert(self):
        treeInsert(self.n, 6)  #Kind rechts eingef�gt
        self.assertEqual(self.n.right.key, 6)
        treeInsert(self.n, 4)  #Kind links eingef�gt
        self.assertEqual(self.n.left.key, 4)
        node1 = self.n.left    #Alten Knoten speichern
        treeInsert(self.n, 4)  #Neuen Knoten mit gleichem Wert anlegen...
        node2 = self.n.left    #...und speichern
        self.assertIs(node1, node2) #�berpr�fung ob immernoch das gleiche Objekt

    def test_reePredecessor(self):
        treeInsert(self.n, 4)
        treeInsert(self.n, 3)
        node = treePredecessor(self.n.left)
        self.assertEqual(node.key, 3)

    def test_reeHasKey(self):
        treeInsert(self.n, 6)
        treeInsert(self.n, 4)
        pos = treeHasKey(self.n, 800)
        self.assertIsNone(pos)         #Key nicht vorhanden

        pos = treeHasKey(self.n, 6)
        self.assertIs(pos, self.n.right) #Key im rechten Teilbaum gefunden

        pos = treeHasKey(self.n, 4)
        self.assertIs(pos, self.n.left) #Key im linken Teilbaum gefunden
        
        pos = treeHasKey(self.n, 5)
        self.assertIs(pos, self.n)      #Key ist Wurzel
        
    def test_treeRemove(self):
        treeInsert(self.n, 6)       #Vorkonfiguration starten
        treeInsert(self.n, 8)
        treeInsert(self.n, 7)
        treeInsert(self.n, 3)   
        treeInsert(self.n, 4)
        self.assertRaises(RuntimeError,treeRemove(self.n, 18))

        treeRemove(self.n, 4)      #Blatt entfernen
        self.assertIsNone(self.n.left.right)

        treeInsert(self.n, 4)      #Ursprungsbaum wiederherstellen
        treeRemove(self.n, 3)      #Knoten mit rechtem Kind l�schen
        self.assertEqual(self.n.left.key, 4)

        treeRemove(self.n, 8)      #Knoten mit linkem Kind l�schen
        self.assertEqual(self.n.right.right.key, 7)

        treeInsert(self.n, 2)
        treeInsert(self.n, 1)
        treeInsert(self.n, 3)      #Zwei Kinder an 2 anh�ngen
        treeRemove(self.n, 2)      #Knoten mit zwei Kindern l�schen
        self.assertEqual(self.n.left.left.key, 1)       #Linker Knoten rutscht hoch
        self.assertEqual(self.n.left.left.right.key, 3) #Rechter Knoten wird Kind von 1

    def test_treeDepth(self):
        treeInsert(self.n, 6)       #Vorkonfiguration starten
        treeInsert(self.n, 8)       #Teilweise ausgeglichen
        treeInsert(self.n, 3)   
        treeInsert(self.n, 4)
        treeInsert(self.n, 2)
        treeInsert(self.n, 9)

        depth = treeDepth(self.n)
        self.assertEqual(depth, 4)  #Tiefe entspricht 3


        self.p = Node(5)
        treeInsert(self.p, 4)       #V�llig unausgeglichen
        treeInsert(self.p, 3)
        treeInsert(self.p, 2)
        treeInsert(self.p, 1)
        depth = treeDepth(self.p)
        self.assertEqual(depth, 5)


        self.o = Node(5)            #V�llig ausgeglichen
        treeInsert(self.o, 3)       
        treeInsert(self.o, 4)
        treeInsert(self.o, 2)
        treeInsert(self.o, 7)
        treeInsert(self.o, 6)
        treeInsert(self.o, 8)
        depth = treeDepth(self.o)
        self.assertEqual(depth, 3)

if __name__ == '__main__':
    unittest.main()

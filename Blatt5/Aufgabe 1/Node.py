# -*- coding: cp1252 -*-
class Node:
    def __init__(self, key):
        self.key = key
        self.left = None
        self.right = None

def treeInsert(node, key):
    if node is None:      # richtiger Platz gefunden
        return Node(key)  # => neuen Knoten einf�gen
    if node.key == key:   # schon vorhanden
        return node       # => nichts tun
    elif key < node.key:     
        node.left = treeInsert(node.left, key) # im linken Teilbaum einf�gen
    else:
        node.right = treeInsert(node.right, key) # im rechten Teilbaum einf�gen
    return node

def treePredecessor(node):
    node = node.left
    while node.right is not None:
        node = node.right
    return node

def treeRemove(node, key):
        try:
            if treeHasKey(node, key) is None:
                    raise RuntimeError("Key nicht vorhanden!")
            else:
                    if node is None:   # key nicht vorhanden	
                            return node    # => nichts tun
                    if key < node.key: 
                            node.left = treeRemove(node.left, key)
                    elif key > node.key:
                            node.right = treeRemove(node.right, key)
                    else:              # key gefunden
                            if node.left is None and node.right is None:     # Fall 1
                                    node = None            
                            elif node.left is None:     # Fall 2
                                    node = node.right       # +
                            elif node.right is None:    # Fall 2
                                    node = node.left
                            else:                       # Fall 3
                                    pred = treePredecessor(node)
                                    node.key = pred.key
                                    node.left = treeRemove(node.left, pred.key)
                    return node
        except RuntimeError as e:
            print e
	    
def treeHasKey(node, key):
    if node is None:
        return None
    elif node.key == key: # gefunden
        return node       # => Knoten zur�ckgeben
    elif key < node.key:  # gesuchter Schl�ssel ist kleiner
        return treeHasKey(node.left, key)  # => im linken Unterbaum weitersuchen
    else:                 # andernfalls 
        return treeHasKey(node.right, key) # => im rechten Unterbaum weitersuchen

def treeDepth(node):
    if node is None:
        return 0            #Keine Tiefe, da gar kein Knoten
    elif node.left is None and node.right is None:
        return 1            #Tiefe = 1, da nur eine Wurzel enthalten
    return max(treeDepth(node.left), treeDepth(node.right)) + 1

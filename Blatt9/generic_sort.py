# -*- coding: cp1252 -*-

import unittest

array0=[4,1,3,2,5]
array1=[-5,-2,5,4,-8]
array2=[1,4,3,5,7,2,6]

def myOrdering(x,y):
    return y-x

def abs_abst(x,y):
    x_abs=abs(x)
    y_abs=abs(y)
    return y_abs-x_abs

def auf_ger_ab_unger(x,y):
    if x%2==0 and y%2==0:
        return x-y
    elif x%2!=0 and y%2!=0:
        return y-x
    elif x%2==0 and y%2!=0:
        return -1
    elif x%2!=0 and y%2==0:
        return 1

print "array0 unsortiert:",array0
array0.sort(myOrdering)
print "array0 absteigend:",array0

print "==============="

print "array1 unsortiert:",array1
array1.sort(abs_abst)
print "array1 absteigende Betr�ge:",array1

print "==============="

print "array2 unsortiert:",array2
array2.sort(auf_ger_ab_unger)
print "array2 auf_ger_ab_unger:",array2


